FROM resin/raspberrypi2-debian

RUN apt-get --yes update
RUN apt-get --yes install sqlite3
RUN sqlite3 debianData.db
